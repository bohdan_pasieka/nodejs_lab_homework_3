const {Schema, model} = require('mongoose')

const Load = new Schema({
    created_by: {
       type: Schema.Types.ObjectId,
       required: true
    },
    assigned_to: {
        type: Schema.Types.ObjectId,
        default: null
    },
    status: {
        type: String,
        default: 'NEW'
    },
    state: {
        type: String,
        default: null
    },
    name: {
        type: String,
        required: true
    },
    payload: {
        type: Number,
        required: true
    },
    pickup_address: {
        type: String,
        required: true
    },
    delivery_address: {
        type: String,
        required: true
    },
    dimensions: {
      width: {
        type: Number,
        required: true
        },
        length: {
            type: Number,
            required: true
        },
        height: {
            type: Number,
            required: true
        }
    },
    logs:[{
        _id: false,
        message: {
            type: String,
            required: true
        },
          time: {
            type: Date,
            dafault: Date.now()
        }
    }],
    created_date: {
        type: Date,
        dafault: Date.now()
    }
})

module.exports = model('load', Load)