const Truck = require('../models/truckModel')
const User = require('../models/authModel')

const getTrucks = async (req, res) => {
    const {_id} = req.user

    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'DRIVER') {
       const trucks = await Truck.find({created_by: _id},{__v: 0}, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }})
        res.json({trucks})
    } else {
        res.status(400).json({message: 'Shipper can not get trucks'})
    } 
}

const addTruck = async (req, res) => {
    const {_id} = req.user
    const {type} = req.body
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }
    if (user.role === 'DRIVER') {
        const truck = new Truck({created_by:_id, type, created_date: Date.now()})
        await truck.save()
        res.status(200).json({message: "Truck created successfully"})
    } else {
        res.status(400).json({message: 'Shipper can not create a truck'})
    } 
}
 
const getTruckById = async (req, res) => {
    const {_id} = req.user
    const {id} = req.params
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'DRIVER') {
       const truck = await Truck.findById(id, {__v: 0}, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }})
        if (!truck) {
            res.status(400).json({message: 'Truck not found'})
            return
        }
        res.json({truck})
    } else {
        res.status(400).json({message: 'Shipper can not get a truck'})
    }
}  

const updateTruckById = async (req, res) => {
    const {_id} = req.user
    const {type} = req.body
    const {id} = req.params
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'DRIVER') {
       await Truck.findByIdAndUpdate(id, {type}, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }})
        res.json({message: "Truck details changed successfully"})
    } else {
        res.status(400).json({message: 'Shipper can not update a truck'})
    }
}


const deleteTruckById = async (req, res) => {
    const {_id} = req.user
    const {id} = req.params
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'DRIVER') {
       await Truck.findByIdAndDelete(id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }})
        res.json({message: "Truck deleted successfully"})
    } else {
        res.status(400).json({message: 'Shipper can not delete a truck'})
    }
}

const assignTruckById = async (req, res) => {
    const {_id} = req.user
    const {id} = req.params
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'DRIVER') {
       await Truck.findByIdAndUpdate(id, {assigned_to:_id}, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }})
        res.json({message: "Truck assigned successfully"})
    } else {
        res.status(400).json({message: 'Shipper can not assign a truck'})
    }
}

module.exports = {getTrucks, getTruckById, addTruck, updateTruckById, assignTruckById, deleteTruckById}
