const {Router} = require('express')
const {getProfileInfo, deleteProfile, changePassword} = require('../controllers/userController')
const { tokenMiddleware } = require('../middleware/tokenMiddleware') 
const { checkOldPassword, checkNewPassword } = require('../middleware/changePasswordMiddleware') 

const router = Router()

router.get('/', [tokenMiddleware], getProfileInfo)
router.delete('/',[tokenMiddleware], deleteProfile)
router.patch('/password', [tokenMiddleware, checkOldPassword, checkNewPassword], changePassword)

module.exports = router