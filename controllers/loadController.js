const User = require('../models/authModel')
const Load = require('../models/loadModel')
const Truck = require('../models/truckModel')

const getLoads = async (req, res) => {
    const {_id} = req.user
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'SHIPPER') {
        const loads = await Load.find({created_by: _id}, {__v: 0})
        res.status(200).json({loads})
    } else {
        res.status(400).json({message: 'Driver can not get loads'})
    }
}

const addLoad = async (req, res) => {
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body
    const {_id} = req.user
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'SHIPPER') {
        const load = new Load({ 
            created_by:_id, 
            name, 
            payload, 
            pickup_address, 
            delivery_address, 
            dimensions: {
                width: dimensions.width,
                length: dimensions.length,
                height: dimensions.height
            },
            logs:[{
                message: "Load assigned to driver with id ###",
                time: Date.now()
            }],
            created_date: Date.now()
        })
 
        await load.save()
        res.status(200).json({message: "Load created successfully"})
    } else {
        res.status(400).json({message: 'Driver can not create a load'})
    }
}

const getActiveLoads = async (req, res) => {
    const {_id} = req.user
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }
    const truck = await Truck.findOne({created_by: user._id, status: 'OL'}, err => {
        if (err) {
            res.status(400).json({message: 'Load not found'})
            return
        }
    })

    if (user.role === 'DRIVER') {
       const load = await Load.findOne({assigned_to: truck._id}, {__v: 0}, err => {
        if (err) {
            res.status(400).json({message: 'Load not found'})
            return
        }})
        if (!load) {
            res.status(400).json({message: 'Load not found'})
            return
        }
        res.json({load})
    } else {
        res.status(400).json({message: 'Shipper can not get an active load'})
    }
} 

const iterateNextLoadState = async (req, res) => {
    const {_id} = req.user
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }
    const truck = await Truck.findOne({created_by: user._id, status: 'OL'}, err => {
        if (err) {
            res.status(400).json({message: 'Load not found'})
            return
        }
    })

    if (user.role === 'DRIVER') {
        let message
        const load = await Load.findOne({assigned_to: truck._id}, err => {
        if (err) {
            res.status(400).json({message: 'Load not found'})
            return
        }})
        if(load.state === 'En route to Pick Up') {
            load.state = 'Arrived to Pick Up'
            message = 'Arrived to Pick Up'
        } else if(load.state === 'Arrived to Pick Up') {
            load.state = 'En route to delivery'
            message = 'En route to delivery'
        }else if(load.state === 'En route to delivery') {
            load.state = 'Arrived to delivery'
            message = 'Arrived to delivery'
            load.status = 'SHIPPED'
            await Truck.findOneAndUpdate({created_by: user._id}, {status: 'IS'}, err => {
                if (err) {
                    res.status(400).json({message: 'Load not found'})
                    return
                }
            }) 
        }

        await load.save()
        res.json({message: `Load state changed to ${message}`})
    } else {
        res.status(400).json({message: 'Shipper can not get an active load'})
    } 
}

const getLoadById = async (req, res) => {
    const {id} = req.params
    const {_id} = req.user
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'SHIPPER') {
        const load = await Load.findById(id, {__v: 0}, err => {
            if (err) {
                res.status(400).json({message: 'Load not found'})
                return
            }
        })

        if (!load) {
            res.status(400).json({message: 'Load not found'})
            return
        }
        res.status(200).json({load})
    } else {
        res.status(400).json({message: 'Driver can not get loads'})
    }
} 

const updateLoadById = async (req, res) => {
    const {_id} = req.user
    const {id} = req.params
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'SHIPPER') {
        await Load.findByIdAndUpdate(id, 
            {   
                name, 
                payload, 
                pickup_address, 
                delivery_address, 
                dimensions: {
                    width: dimensions.width,
                    length: dimensions.length,
                    height: dimensions.height
                }
            }, 
            err => {
                if (err) {
                    res.status(400).json({message: 'Load not found'})
                }
        })

        res.status(200).json({message: 'Load details changed successfully'})
    } else {
        res.status(400).json({message: 'Driver can not get loads'})
    }
}

const deleteLoadById = async (req, res) => {
    const {_id} = req.user
    const {id} = req.params
    const user = await User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'User not found'})
            return
        }
    })

    if (!user) {
        res.status(400).json({message: 'User not found'})
        return
    }

    if (user.role === 'SHIPPER') {
        await Load.findByIdAndDelete(id, {__v: 0}, err => {
            if (err) {
                res.status(400).json({message: 'Load not found'})
                return
            }
        })

        res.status(200).json({message: "Load deleted successfully"})
    } else {
        res.status(400).json({message: 'Driver can not delete load'})
    }
}

const postLoadById = async (req, res) => {
    const {id} = req.params
    const trucks = await Truck.find({status: 'IS', assigned_to: { $ne: null }}, err => {
        if (err) {
            res.status(400).json({message: 'Not found a truck'})
            return
        }
    })
    const i = Math.floor(Math.random() * trucks.length)

    if (!trucks.length) {
        res.status(400).json({message: 'Not found a truck'})
        return
    }

    const truck = trucks[i]._id

    await Load.findByIdAndUpdate(id, {status: 'POSTED'}, err => {
        if (err) {
            res.status(400).json({message: 'Not found a load'})
            return
        }
    })

    await Truck.findByIdAndUpdate(truck, {status: 'OL'}, async (err) => {
        if (err) {
            await Load.findByIdAndUpdate(id, {status: 'NEW'}, err => {
                if (err) {
                    res.status(400).json({message: 'Not found a load'})
                    return
                }
            })
         res.status(400).json({message: 'Not found a truck'})
         return
        }
    })

    await Load.findByIdAndUpdate(id, {status: 'ASSIGNED', state: 'En route to Pick Up', assigned_to: truck, 
    logs:[{
        message: `Load assigned to driver with id ${trucks[i].created_by}`,
        time: Date.now()
    }]
    }, err => {
        if (err) {
            res.status(400).json({message: 'Not found a load'})
            return
        }
        res.status(200).json({ 
            message: 'Load posted successfully',
            driver_found: true
        })
    })

}

const getShippingInfo = async (req, res) => {
    const {id} = req.params
    const idTruck = await Load.findById(id, err => {
        if (err) {
            res.status(400).json({message: 'Not found a load'})
        }
    })
    const load = await Load.findById(id, {__v: 0}, err => {
        if (err) {
            res.status(400).json({message: 'Not found a load'})
            return
        }
    })
    let truck
    try {
            truck = await Truck.findById(idTruck.assigned_to, {__v: 0}, err => {
            if (err) {
                res.status(400).json({message: 'Not found a truck'})
                return
            }
        })
    } catch (error) {
        res.status(400).json({message: 'Not found a truck'})
        return
    }
    
    res.status(200).json({ 
        load,
        truck
    })
}

module.exports = {   
    getLoads, 
    addLoad, 
    getActiveLoads, 
    iterateNextLoadState,
    getLoadById, 
    updateLoadById,
    deleteLoadById, 
    postLoadById, 
    getShippingInfo
}