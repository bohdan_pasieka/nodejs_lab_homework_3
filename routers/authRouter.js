const {Router} = require('express')
const {login, register, forgotPassword} = require('../controllers/authController')
const {checkCredentials} = require('../middleware/checkCredentials')

const router = Router()

router.post('/login', login)
router.post('/register', checkCredentials, register)
router.post('/forgot_password', forgotPassword)

module.exports = router