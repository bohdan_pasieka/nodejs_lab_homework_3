const {Router} = require('express')
const { tokenMiddleware } = require('../middleware/tokenMiddleware')
const {   
    getLoads, 
    addLoad, 
    getActiveLoads, 
    iterateNextLoadState,
    getLoadById, 
    updateLoadById,
    deleteLoadById, 
    postLoadById, 
    getShippingInfo
} = require('../controllers/loadController')

const router = Router()

router.get('/', [tokenMiddleware], getLoads)
router.post('/', [tokenMiddleware], addLoad)
router.get('/active', [tokenMiddleware], getActiveLoads)
router.patch('/active/state', [tokenMiddleware], iterateNextLoadState)
router.get('/:id', [tokenMiddleware], getLoadById)
router.put('/:id', [tokenMiddleware], updateLoadById)
router.delete('/:id', [tokenMiddleware], deleteLoadById)
router.post('/:id/post', [tokenMiddleware], postLoadById)
router.get('/:id/shipping_info', [tokenMiddleware], getShippingInfo)

 
module.exports = router