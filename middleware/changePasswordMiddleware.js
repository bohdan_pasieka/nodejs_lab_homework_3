const Joi = require('joi')
const bcrypt = require('bcrypt')
const User = require('../models/authModel')

const checkOldPassword = async (req, res, next) => {
    const {_id} = req.user
    const {oldPassword} = req.body
    const user = await User.findById(_id, (err) => {
        if (err) {
            res.status(500).json({message: 'Error'})
        } 
    })    

    if (!(await bcrypt.compare(oldPassword, user.password)) ) {
        res.status(400).json({message: 'Old password is not matched'})
        return
    }
    next() 
}  

const checkNewPassword = async (req, res, next) => {
    const schema = Joi.object({
        newPassword: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    })
    try {
        await schema.validateAsync(req.newPassword)
        next()
    } catch (error) {
        res.status(400).json({message: 'Invalid input data'})
    }
}

module.exports = {checkOldPassword, checkNewPassword}