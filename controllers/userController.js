const User = require('../models/authModel')
const bcrypt = require('bcrypt')

const getProfileInfo = async (req, res) => {
    const {_id} = req.user
    await User.findById(_id, (err, info) => {
        if (err) {
            res.status(500).json({message: 'Error'})
            return
        }
        const user = info.toObject()
        delete user.password
        delete user.__v
        res.json({user})
    })
      
}

const deleteProfile = async (req, res) => {
    const {_id} = req.user
    const user = User.findById(_id, err => {
        if (err) {
            res.status(400).json({message: 'Error'})
        }
    })
    if (user.role === 'SHIPPER') {
        try {
            await User.findByIdAndDelete(_id, (err, user) => {
                if (err) {
                    res.status(500).json({message: 'Error'})
                }
            })
        } catch (error) {
            res.status(400).json({message: 'Error'})
        }
        res.status(200).json({message: 'User has been deleted'})
    } else {
        res.status(400).json({message: 'Driver can not delete profile'})
    }
}

const changePassword = async (req, res) => {
    const {_id} = req.user
    const {newPassword} = req.body
    try {
        await User.findByIdAndUpdate(_id,
            {
                password: await bcrypt.hash(newPassword, 10)
            },
            (err) => {
    
            if (err) {
                res.status(500).json({message: 'Error'})
            }
        })
    } catch (error) {
        res.status(400).json({message: 'Error'})
    }
    
    res.status(200).json({message: 'Password is updated'})
}  

module.exports = {getProfileInfo, deleteProfile, changePassword}