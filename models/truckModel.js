const {Schema, model} = require('mongoose')

const Truck = new Schema({
    created_by: {
       type: Schema.Types.ObjectId,
       required: true
    },
    assigned_to: {
        type: Schema.Types.ObjectId,
        default: null
    },
    status: {
        type: String,
        default: "IS"
    },
    type:{
        type: String,
        required: true
    },
    created_date: {
        type: Schema.Types.Date,
        dafault: Date.now()
    }
})

module.exports = model('truck', Truck)