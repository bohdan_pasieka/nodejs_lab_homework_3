const {Schema, model} = require('mongoose')

const User = new Schema({
    email: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        type: String,
        require: true
    },
    role:{
        type: String,
        required: true
    }
    ,
    created_date: {
        type: Date,
        dafault: Date.now()
    }
})

module.exports = model('user', User)