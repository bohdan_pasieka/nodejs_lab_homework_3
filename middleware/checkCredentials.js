const Joi = require('joi')

const checkCredentials = async (req, res, next) => {
    console.log(req.body)
    const schema = Joi.object({
        email: Joi.string()
            .max(30)
            .required()
            .email(),
        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
        role: Joi.string()
        .max(30) 
        .required()  
    })
    try {
        await schema.validateAsync(req.body)
        next()
    } catch (error) {
        res.status(400).json({message: 'Invalid input data'})
    }
}

module.exports = {checkCredentials}