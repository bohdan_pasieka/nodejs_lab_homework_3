const {Router} = require('express')
const { tokenMiddleware } = require('../middleware/tokenMiddleware')
const router = Router()
const {getTrucks, getTruckById, addTruck, updateTruckById, assignTruckById, deleteTruckById} = require('../controllers/truckController')

router.get('/', [tokenMiddleware], getTrucks)
router.post('/', [tokenMiddleware], addTruck) 
router.get('/:id', [tokenMiddleware], getTruckById) 
router.delete('/:id', [tokenMiddleware], deleteTruckById) 
router.put('/:id', [tokenMiddleware], updateTruckById)
router.post('/:id/assign', [tokenMiddleware], assignTruckById)

module.exports = router