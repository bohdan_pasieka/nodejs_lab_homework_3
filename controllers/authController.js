const bcrypt = require('bcrypt')
const User = require('../models/authModel')
const jwt = require ('jsonwebtoken')
const {JWT_SECRET} = require('../config')

const login = async (req, res) => {
    const {email, password} = req.body
    const user = await User.findOne({email}, (err) => {
        if (err) {
            res.status(400).json({message: 'User was not found'})
            return
        }
    }) 
    if (!user) {
        res.status(400).json({message: 'User was not found'})
        return
    }

    if (!(await bcrypt.compare(password, user.password))) {
        res.status(400).json({message: 'Wrong password'})
    }

    const token = jwt.sign({ email: user.email, _id: user._id }, JWT_SECRET)
    res.status(200).json({jwt_token: token})
}

const register = async (req, res) => {
    const {email, password, role} = req.body
   
    const user = await User.findOne({email: email})
    if (!user) {
        const cryptPassword = await bcrypt.hash(password, 10)
        const newUser = new User({email, password: cryptPassword, role})
        await newUser.save()

        res.status(200).json({message: 'Profile created successfully'})    
    } else {
        res.status(400).json({message: 'User with this username is already created'})  
    }
} 

const forgotPassword = async (req, res) => {
    const {email} = req.body
   const user = await User.findOne({email}, (err) => {
        if (err) {
            res.status(400).json({message: 'User was not found'})
            return
        }
    }) 

    if (!user) {
        res.status(400).json({message: 'User was not found'})
        return
    }

    res.status(200).json({message: 'New password sent to your email address'})
}

module.exports = {login, register, forgotPassword}