const express = require('express')
const mongoose = require('mongoose')
const authRouter = require('./routers/authRouter')
const loadRouter = require('./routers/loadRouter')
const truckRouter = require('./routers/truckRouter')
const userRouter = require('./routers/userRouter')

const app = express()
const PORT  = process.env.PORT || 8080
app.use(express.json())

app.use('/api/auth', authRouter)
app.use('/api/loads', loadRouter)
app.use('/api/trucks', truckRouter)
app.use('/api/users/me', userRouter)



const start = async () => {  
    await mongoose.connect('mongodb+srv://bohdan:1234@cluster0.jykeg.mongodb.net/Uber?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    })
    app.listen(PORT, () => {console.log(`Server has been launched at port ${PORT}`)})
}
start()
